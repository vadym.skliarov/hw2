const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const controller = require('../controllers/notes.conroller');
const utils = require('../utils/utils');

router.get('/', utils.authenticateToken, controller.getNotes);

router.post('/', utils.authenticateToken, controller.addNote);

router.get('/:id', utils.authenticateToken, controller.getNote);

router.put('/:id', utils.authenticateToken, controller.modifyNote);

router.patch('/:id', utils.authenticateToken, controller.changeComplete);

router.delete('/:id', utils.authenticateToken, controller.deleteNote);

module.exports = router;
