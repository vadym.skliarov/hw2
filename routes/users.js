const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const controller = require('../controllers/users.controller');
const utils = require('../utils/utils');

router.get('/me', utils.authenticateToken, controller.getUser);

router.delete('/me', utils.authenticateToken, controller.deleteUser);

router.patch('/me', utils.authenticateToken, controller.changePassword);

module.exports = router;
