const jwt = require('jsonwebtoken');
const tokenSecret = process.env.TOKEN_SECRET;

exports.generateToken = (user) => {
  return jwt.sign({id: user._id, username: user.username}, tokenSecret);
};

exports.authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token === null) return res.status(400).send({message: 'Error'});

  jwt.verify(token, tokenSecret, (err, user) => {
    if (err) return res.status(400).send({message: 'Error'});
    req.user = user;
    next();
  });
};
