const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const model = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
}, {collection: 'notes', versionKey: false});

// eslint-disable-next-line new-cap
module.exports = new mongoose.model('Note', model);
