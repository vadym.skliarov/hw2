const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const model = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
}, {collection: 'users', versionKey: false});

// eslint-disable-next-line new-cap
module.exports = new mongoose.model('User', model);
