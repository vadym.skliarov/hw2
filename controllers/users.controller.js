const User = require('../models/user');
const Note = require('../models/note');
const bcrypt = require('bcrypt');
const morgan = require('morgan');
const rounds = 10;

exports.getUser= async (req, res) => {
  try {
    const userJWT = req.user;
    const id = userJWT.id;
    const user = await User.findOne({id});
    const userWithoutPass = {
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    };
    morgan.token('body', (req) => JSON.stringify({user: userWithoutPass}));
    return res.status(200).send({user: userWithoutPass});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const userJWT = req.user;
    const id = userJWT.id;
    const user = await User.findOne({userId: id});

    await user.remove();
    await Note.deleteMany({id});
    morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
    return res.status(200).send({message: 'Success'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.changePassword = async (req, res) => {
  try {
    const {newPassword, oldPassword} = req.body;
    const userJWT = req.user;
    const id = userJWT.id;
    const user = await User.findOne({id});

    if (!oldPassword || typeof oldPassword !== 'string') {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Invalid password'});
      });
      return res.status(400).send({message: 'Invalid password'});
    }
    if (oldPassword.length < 5) {
      morgan.token('body', (req) => {
        return JSON.stringify({
          message: 'Password too small. Should be atleast 6 characters',
        });
      });
      return res.status(400).send({
        message: 'Password too small. Should be atleast 6 characters',
      });
    }

    if (await bcrypt.compare(oldPassword, user.password)) {
      hashedPassword = await bcrypt.hash(newPassword, rounds);
      await User.updateOne(
          {id},
          {
            $set: {password: hashedPassword},
          },
      );
      morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
      return res.status(200).send({message: 'Success'});
    }
    morgan.token('body', (req) => JSON.stringify({message: 'Wronge password'}));
    return res.status(400).send({message: 'Wronge password'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
