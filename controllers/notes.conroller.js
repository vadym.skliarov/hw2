const Note = require('../models/note');
const morgan = require('morgan');

exports.addNote = async (req, res) => {
  try {
    const text = req.body.text;
    if (!text.trim()) {
      morgan.token('body', (req) => JSON.stringify({message: 'Text is empty'}));
      return res.status(400).send({message: 'Text is empty'});
    }
    const user = req.user;
    const createdDate = new Date().toJSON();
    await Note.create({
      text,
      userId: user.id,
      completed: false,
      createdDate,
    });
    morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
    return res.status(200).send({message: 'Success'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getNote = async (req, res) => {
  try {
    const id = req.params.id;
    const note = await Note.findById(id);
    morgan.token('body', (req) => JSON.stringify(note));
    return res.status(200).send(note);
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.getNotes = async (req, res) => {
  try {
    const offset = req.query.offset ?? 0;
    const limit = req.query.limit ?? 0;
    const user = req.user;
    const notes = await Note.find({userId: user.id}).skip(offset).limit(limit);
    const result = {
      offset: +offset,
      limit: +limit,
      count: notes.length,
      notes,
    };
    morgan.token('body', (req) => JSON.stringify(result));
    return res.status(200).send(result);
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.modifyNote = async (req, res) => {
  try {
    const id = req.params.id;
    const text = req.body.text;
    await Note.updateOne({_id: id}, {text});
    morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
    return res.status(200).send({message: 'Success'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.changeComplete = async (req, res) => {
  try {
    const id = req.params.id;
    const note = await Note.findById(id);

    await Note.updateOne({_id: id}, {completed: !note.completed});
    morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
    return res.status(200).send({message: 'Success'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
exports.deleteNote = async (req, res) => {
  try {
    const id = req.params.id;
    const note = await Note.findById(id);

    await note.remove();
    morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
    return res.status(200).send({message: 'Success'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};
