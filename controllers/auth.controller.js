const User = require('../models/user');
const bcrypt = require('bcrypt');
const rounds = 10;
const utils = require('../utils/utils');
const morgan = require('morgan');

exports.login = async (req, res) => {
  const {username, password} = req.body;
  try {
    const user = await User.findOne({username});
    if (!user) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Invalid username/password'});
      });
      return res.status(400).send({message: 'Invalid username/password'});
    }
    if (await bcrypt.compare(password, user.password)) {
      const token = utils.generateToken(user);

      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Success', jwt_token: token});
      });
      return res.status(200).send({message: 'Success', jwt_token: token});
    }

    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid username/password'});
    });
    return res.status(400).send({message: 'Invalid username/password'});
  } catch (error) {
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
};

exports.signup = async (req, res) => {
  const {username, password: plainTextPassword} = req.body;

  if (!username || typeof username !== 'string') {
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid username'});
    });
    return res.status(400).send({message: 'Invalid username'});
  }

  if (!plainTextPassword || typeof plainTextPassword !== 'string') {
    morgan.token('body', (req) => {
      return JSON.stringify({message: 'Invalid password'});
    });
    return res.status(400).send({message: 'Invalid password'});
  }

  if (plainTextPassword.length < 5) {
    morgan.token('body', (req) => {
      return JSON.stringify({
        message: 'Password too small. Should be atleast 6 characters',
      });
    });
    return res.status(400).send({
      message: 'Password too small. Should be atleast 6 characters',
    });
  }

  const password = await bcrypt.hash(plainTextPassword, rounds);
  const createdDate = new Date().toJSON();
  try {
    await User.create({
      username,
      createdDate,
      password,
    });
  } catch (error) {
    if (error.code === 11000) {
      morgan.token('body', (req) => {
        return JSON.stringify({message: 'Username already in use'});
      });
      return res.status(400).send({message: 'Username already in use'});
    }
    morgan.token('body', (req) => JSON.stringify({message: 'Error'}));
    return res.status(500).send({message: 'Error'});
  }
  morgan.token('body', (req) => JSON.stringify({message: 'Success'}));
  return res.status(200).send({message: 'Success'});
};
